<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Collection;
use App\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\ProductRequest;


class ProductController extends Controller
{
	public function index(){

		$products = Product::all();
		return view('admin.product.index', compact(['products']));
	}

	public function create(){
		$categories = Collection::pluck('name', 'id');

		return view('admin.product.create', compact(['categories']));
	}

	public function store(ProductRequest $request){

		$product = new Product();
		$product->name = $request->name;

		if ($request->main_image != null) {

            $files = $request->main_image;
            $file_path = date("M-Y");
            $destinationPath =app()->make('path.public');
            $image_path = '/uploads/products/'.$file_path;
            $filename= str_random(3).'cover_'.$files->getClientOriginalName();
            $files->move($destinationPath.$image_path, $filename);            

            $product->image_name = $filename;
            $product->image_path = $image_path;
        }
		
		$product->category_id = $request->category_id;
		$product->price = $request->price;
		$product->description = $request->description;
		$product->technical = $request->technical;
		$product->question = $request->question;
		$product->save();

		for($i=0; $i<count($request->other_image); $i++){

			$productImage = new ProductImage();
			$productImage->product_id = $product->id;

			if ($request->other_image[$i] != null) {

	            $files = $request->other_image[$i];
	            $file_path = date("M-Y");
	            $destinationPath =app()->make('path.public');
	            $image_path = '/uploads/products/'.$file_path;
	            $filename= str_random(3).'cover_'.$files->getClientOriginalName();
	            $files->move($destinationPath.$image_path, $filename);     

	            $productImage->image_name = $filename;
            	$productImage->image_path = $image_path;

	        }
			
			$productImage->save();
		}


		return redirect()->route('admin.product.index');
	}


	public function edit($id){

		$product = Product::findOrFail($id);
		$categories = Collection::pluck('name', 'id');
		$product_image = ProductImage::where('product_id',$id)->get();

		return view('admin.product.edit', compact(['categories','product','product_image']));
	}

	public function update(ProductRequest $request,$id){
		$product = Product::find($id);
		$product->name = $request->name;

		if ($request->main_image != null) {

            $files = $request->main_image;
            $file_path = date("M-Y");
            $destinationPath =app()->make('path.public');
            $image_path = '/uploads/products/'.$file_path;
            $filename= str_random(3).'cover_'.$files->getClientOriginalName();
            $files->move($destinationPath.$image_path, $filename);          

            $product->image_name = $filename;
            $product->image_path = $image_path;
        }
		
		$product->category_id = $request->category_id;
		$product->price = $request->price;
		$product->description = $request->description;
		$product->technical = $request->technical;
		$product->question = $request->question;
		$product->save();
		
		//update existing image
		if($request->other_image != NULL){
			foreach($request->other_image as $key=>$value){
				$files = $value;
	            $file_path = date("M-Y");
	            $destinationPath =app()->make('path.public');
	            $image_path = '/uploads/products/'.$file_path;
	            $filename= str_random(3).'cover_'.$files->getClientOriginalName();
	            $files->move($destinationPath.$image_path, $filename);          

				ProductImage::where('id',$key)->update([
					'image_name' => $filename,
					'image_path' => $image_path
				]);
			}
			
		}

		//save new image
		if($request->other_new_image != NULL){
			foreach($request->other_new_image as $key=>$value){
				$files = $value;
	            $file_path = date("M-Y");
	            $destinationPath =app()->make('path.public');
	            $image_path = '/uploads/products/'.$file_path;
	            $filename= str_random(3).'cover_'.$files->getClientOriginalName();
	            $files->move($destinationPath.$image_path, $filename);

				ProductImage::create([
					'product_id' => $product->id,
					'image_name' => $filename,
					'image_path' => $image_path
				]);
			}
			
		}

		return redirect()->route('admin.product.index');
	}

	public function show($id){

		$product = Product::findOrFail($id);
		$categories = Collection::pluck('name', 'id');
		$product_image = ProductImage::where('product_id',$id)->get();

		return view('admin.product.view', compact(['categories','product','product_image']));
	}

	public function deleteImage($image_id,$product_id){

		ProductImage::where('id',$image_id)->where('product_id',$product_id)->delete();
		return "Ok";
	}

	public function destroy($id)
    {
        
        $product = Product::findOrFail($id);
        $product->delete();

        $product_image = ProductImage::where('product_id', $id)->delete();

        return redirect()->route('admin.product.index');
    }
}
