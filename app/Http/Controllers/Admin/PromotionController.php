<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Promotion;
use App\Http\Requests\Admin\PromotionRequest;

class PromotionController extends Controller
{
    public function index(){

    	$promotions = Promotion::all();
    	return view('admin.promotion.index', compact(['promotions']));
    }

    public function create(){

    	return view('admin.promotion.create');
    }

    public function store(PromotionRequest $request){

    	if ($request->image != null) {

            $files = $request->image;
            $file_path = date("M-Y");
            $destinationPath =app()->make('path.public');
            $image_path = '/uploads/promotion/'.$file_path;
            $filename= str_random(3).'cover_'.$files->getClientOriginalName();
            $files->move($destinationPath.$image_path, $filename);            

            $image_name = $filename;
            $image_path = $image_path;
        }

    	Promotion::create([
    		'title' => $request->title,
    		'description'=> $request->description,
    		'image_name' => $image_name,
    		'image_path' => $image_path
    	]);

    	return redirect()->route('admin.promotion.index');
    }

    public function edit($id){

    	$promotion = Promotion::find($id);
    	return view('admin.promotion.edit',compact(['promotion']));
    }

    public function update(Request $request,$id){
    	if ($request->image != null) {
            $files = $request->image;
            $file_path = date("M-Y");
            $destinationPath =app()->make('path.public');
            $image_path = '/uploads/promotion/'.$file_path;
            $filename= str_random(3).'cover_'.$files->getClientOriginalName();
            $files->move($destinationPath.$image_path, $filename);   

            $image_name = $filename;
            $image_path = $image_path;

            Promotion::where('id',$id)->update([
	    		'title' => $request->title,
	    		'description'=> $request->description,
	    		'image_name' => $image_name,
	    		'image_path' => $image_path
    		]);
        }else{
        	Promotion::where('id',$id)->update([
	    		'title' => $request->title,
	    		'description'=> $request->description,
	    		
    		]);
        }
    	return redirect()->route('admin.promotion.index');
    }

    public function show($id){
    	$promotion = Promotion::find($id);
    	return view('admin.promotion.view',compact(['promotion']));
    }

    public function destroy($id)
    {
        
        $about = Promotion::findOrFail($id);
        $about->delete();

        return redirect()->route('admin.about.index');
    }

    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = Promotion::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
