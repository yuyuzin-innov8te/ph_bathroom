<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Collection;
use App\Product;
use Illuminate\Foundation\Validation\ValidatesRequests;

class CollectionController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collections = Collection::all();
        $count_set_home = Collection::where('show_front_end','yes')->get()->count();
        return view('admin.collection.index', compact(['collections','count_set_home']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_categories = Collection::whereNull('sub_collection')->get();
        return view('admin.collection.create',compact(['main_categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $collection = new Collection();
        $collection->name = $request->name;
        $collection->type = $request->is_collection;
        if ($request->sub_category != null) {
            $collection->sub_collection = $request->sub_category;
        }
        $collection->save();
        return redirect('/admin/collection');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $collections = Collection::find($id);
        return view('admin.collection.view',compact(['collections']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collection = Collection::find($id);
        $main_categories = Collection::whereNull('sub_collection')->get();
        return view('admin.collection.edit',compact(['collection','main_categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //dd($id);
        $collection = Collection::find($id);
        $collection->name = $request->name;
        $collection->type = $request->is_collection;
        if ($request->sub_category != null) {
            $collection->sub_collection = $request->sub_category;
        }
        $collection->save();
        return redirect('/admin/collection');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collection = Collection::find($id);
        $collection->delete();
        $product = Product::where('category_id',$id)->get();
        
        if (count($product)>0) {            
            Product::where('category_id',$id)->delete();
        } 
        
        return redirect('/admin/collection');
    }

    public function setHome($id)
    {
        $category = Collection::where('id',$id)->value('show_front_end');
        $result = '';
            $collection = Collection::find($id);
        if ($category == 'yes') {
            $collection->show_front_end = null;
            $result = - 1;
        }else{
            $collection->show_front_end = 'yes';
            $result = + 1;
        }
            $collection->save();

        return $result;
    }
}
