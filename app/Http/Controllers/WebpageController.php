<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use App\Collection;
use App\Product;
use App\Promotion;
use App\About;
use DB;
use PDF;
use Mail;

class WebpageController extends Controller
{
    public function product_list($id='') {
        if ($id) {
            $main_category_id = $id;
            $products = Product::where('category_id', $main_category_id)->paginate(4);
        }else{
            $products = Product::paginate(4);
        } 
        $categories = DB::table('collections')
        ->select('*')
        ->whereNull('deleted_at')
        ->get();        

        return view('product-list', compact('categories', 'products', 'main_category_id'));
    }

    public function product_filter(Request $request) {
        if ($request->main_category_id) {
         $main_category_id = $request->main_category_id;

         if ($request->filter_by == "z_to_a") {
            $products = Product::where('category_id',$main_category_id)->orderBy('name', 'desc')->get();
        }
        if ($request->filter_by == "a_to_z") {
            $products = Product::where('category_id',$main_category_id)->orderBy('name', 'asc')->get();
        }

        }else {
            if ($request->filter_by == "z_to_a") {
                $products = Product::orderBy('name', 'desc')->get();
            }
            if ($request->filter_by == "a_to_z") {
                $products = Product::orderBy('name', 'asc')->get();
            }
        }
        return $products;
    }

    public function product_view_all(Request $request) {
        if ($request->main_category_id) {
            $main_category_id = $request->main_category_id;
            $products = Product::where('category_id',$main_category_id)->get();
        }else {
            $products = Product::all();
        }
        return $products;
    }

    public function product_detail($id) {
        $product = Product::find($id);
        $product_images = DB::table('product_images')
        ->select('*')
        ->where('product_id',$id)
        ->whereNull('deleted_at')
        ->get();

        $categories = DB::table('collections')
        ->select('*')
        ->whereNull('deleted_at')
        ->get(); 

        $related_products = Product::where('category_id', $product->category_id)
        ->where('id', '!=', $id)
        ->get();
        return view('product-detail', compact('categories', 'product', 'product_images', 'related_products'));
    }

    public function product_price_filter(Request $request) {
        // dd($request->all());
        $min_price  =   $request->min_price;
        $max_price  =   $request->max_price;

        if ($request->main_category_id) {
        $main_category_id = $request->main_category_id;

        $products = Product::where('category_id',$main_category_id)
                    ->whereBetween('price',  [$min_price, $max_price])
                    ->orderBy('name', 'asc')->get();

        }else {
            $products = Product::whereBetween('price',  [$min_price, $max_price])
                        ->orderBy('name', 'asc')->get();
        }

        // dd($products);
        return $products;
    }

    public function about_us() 
    {
     // die('i arrive here');
        $content =DB::table('abouts')->first();            
        return view('about-us',compact(['content']));
    	
    }


    public function contact_us() {

         return view('contact_us');

    }

     public function sendEmail(Request $request) 
    {    

        $to = 'sandi.khaing@innov8te.com.sg';//fosterresources@gmail.com
        $subject = 'Contact us Information';
       
        $data = array('data' => $request->all());
        $mail = Mail::send('admin.email.test', $data, function($message) use ($to, $subject)
        {  
            $message->to($to);
            
            $message->subject($subject);
        });

        return Redirect('/contact-us');     
    	
    }

    public function promotion() 
    {
       $content = DB::table('promotions')->get(); 
        return view('promotion',compact(['content']));
        
    }
}
