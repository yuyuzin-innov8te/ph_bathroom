<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    

    public function rules()
    {
        if ($this::isMethod('post'))
        {
            return $this->getStoreRules();
        }

        if ($this::isMethod('put')) {
            return $this->getUpdateRules();
        }
    }

    public function getStoreRules(){
        return [
            'title' => 'required',
            'image'=> 'required',
        ];
    }

    public function getUpdateRules(){
        return [
            'title' => 'required',
            'image' => 'required'
        ];
    }
}
