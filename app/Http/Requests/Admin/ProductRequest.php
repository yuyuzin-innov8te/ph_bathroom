<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this::isMethod('post'))
        {
            return $this->getStoreRules();
        }

        if ($this::isMethod('put')) {
            return $this->getUpdateRules();
        }
    }

    public function getStoreRules(){
        return [
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'technical' => 'required',
            'question' => 'required',
        ];
    }

    public function getUpdateRules(){
        return [
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'technical' => 'required',
            'question' => 'required',
        ];
    }
}
