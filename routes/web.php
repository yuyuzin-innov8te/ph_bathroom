<?php
Route::get('/', function () { return redirect('/admin/home'); });


Route::get('/product-list/{id?}', 'WebpageController@product_list');

Route::get('/product-detail/{id}', 'WebpageController@product_detail');

Route::post('/product-filter', 'WebpageController@product_filter');

Route::post('/product-view-all', 'WebpageController@product_view_all');

Route::get('/product-price-filter', 'WebpageController@product_price_filter');


Route::get('/about-us', 'WebpageController@about_us');

Route::get('/contact-us', 'WebpageController@contact_us');

Route::post('/sendingemail', 'WebpageController@sendEmail');

Route::get('/promotion', 'WebpageController@promotion');

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);

    Route::resource('collection', 'Admin\CollectionController');

    Route::resource('product', 'Admin\ProductController');

    Route::get('delete_image/{image_id}/{product_id}', 'Admin\ProductController@deleteImage');

    Route::resource('about', 'Admin\AboutController');

    Route::resource('promotion', 'Admin\PromotionController');

});
