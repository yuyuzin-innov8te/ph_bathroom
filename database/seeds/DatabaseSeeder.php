<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(RoleSeed::class);
        $this->call(UserSeed::class);
        $this->call(CollectionsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductImageTableSeeder::class);
        $this->call(AboutTableSeeder::class);
        $this->call(PromotionTableSeeder::class);

    }
}
