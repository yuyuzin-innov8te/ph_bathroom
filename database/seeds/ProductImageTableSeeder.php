<?php

use Illuminate\Database\Seeder;
use App\ProductImage;
use Carbon\Carbon;

class ProductImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $image = [
    		[
    			'id'		 => 1,
    			'product_id' => 1,
    			'image_path' => '/uploads/products/Oct-2017',
            	'image_name' => 'p2.jpg',
            	'deleted_at' => NULL,
	            'created_at' => Carbon::now(),
	            'updated_at' => Carbon::now()
    		],
    		[
    			'id'		 => 2,
    			'product_id' => 1,
    			'image_path' => '/uploads/products/Oct-2017',
            	'image_name' => 'p3.jpg',
            	'deleted_at' => NULL,
	            'created_at' => Carbon::now(),
	            'updated_at' => Carbon::now()
    		],
    		[
    			'id'		 => 3,
    			'product_id' => 1,
    			'image_path' => '/uploads/products/Oct-2017',
            	'image_name' => 'p4.jpg',
            	'deleted_at' => NULL,
	            'created_at' => Carbon::now(),
	            'updated_at' => Carbon::now()
    		],
            [
                'id'         => 4,
                'product_id' => 1,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p5.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 5,
                'product_id' => 2,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p6.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 6,
                'product_id' => 2,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p7.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 7,
                'product_id' => 2,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p8.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 8,
                'product_id' => 2,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p9.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 9,
                'product_id' => 3,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p10.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 10,
                'product_id' => 3,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p1.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 11,
                'product_id' => 3,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p8.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 12,
                'product_id' => 3,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p7.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 13,
                'product_id' => 4,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p1.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 14,
                'product_id' => 4,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p5.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 15,
                'product_id' => 4,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p7.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 16,
                'product_id' => 4,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p3.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 17,
                'product_id' => 5,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p2.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 18,
                'product_id' => 5,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p4.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 19,
                'product_id' => 5,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p7.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 20,
                'product_id' => 5,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p9.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 21,
                'product_id' => 6,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p5.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 22,
                'product_id' => 6,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p7.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 23,
                'product_id' => 6,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p1.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 24,
                'product_id' => 6,
                'image_path' => '/uploads/products/Oct-2017',
                'image_name' => 'p8.jpg',
                'deleted_at' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
    	];
    ProductImage::insert($image);
    }
}
