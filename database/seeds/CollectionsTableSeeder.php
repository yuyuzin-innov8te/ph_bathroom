<?php

use Illuminate\Database\Seeder;

use App\Collection;
use Carbon\Carbon;

class CollectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            [
                'id' => 1,
                'name' => 'Acc',
                'sub_collection' => NULL,
                'show_front_end' => 'yes',
                'type' => 'no',
                'created_at' => '2017-06-07 12:04:03',
                'updated_at' => '2017-06-08 18:15:09',
                'deleted_at' => NULL
            ],
            [
                'id' => 2,
                'name' => 'Basin',
                'sub_collection' => NULL,
                'show_front_end' => 'yes',
                'type' => 'no',
                'created_at' => '2017-06-07 12:40:31',
                'updated_at' => '2017-06-08 18:26:08',
                'deleted_at' => NULL
            ],
            [
                'id' => 3,
                'name' => 'Hand Shower',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => '2017-06-08 19:41:29',
                'deleted_at' => NULL
            ],
            [
                'id' => 4,
                'name' => 'Heater',
                'sub_collection' => NULL,
                'show_front_end' => 'yes',
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => '2017-06-08 19:41:30',
                'deleted_at' => NULL
            ],
            [
                'id' => 5,
                'name' => 'HOB',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 6,
                'name' => 'HOOD',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 7,
                'name' => 'Bottle',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 8,
                'name' => 'Instant Heater',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 9,
                'name' => 'Kitchen Sink',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 10,
                'name' => 'Shower & Bath  Mixer',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 11,
                'name' => 'Tap',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 12,
                'name' => 'Water Closet',
                'sub_collection' => NULL,
                'show_front_end' => NULL,
                'type' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
        ];
        Collection::insert($collections);
    }
}
