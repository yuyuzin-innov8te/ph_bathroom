<?php

use Illuminate\Database\Seeder;
use App\Promotion;
use Carbon\Carbon;

class PromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $about = [
        	[
        		'id'=>1,
        		'title' => 'Promotion1',
        		'image_name' => 'p9.jpg',
        		'image_path' => '/uploads/promotion/Oct-2017' ,
        		'description' => 'Promotion',
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now()
        	],
        	[
        		'id'=>2,
        		'title' => 'Promotion2',
        		'image_name' => 'p8.jpg',
        		'image_path' => '/uploads/promotion/Oct-2017' ,
        		'description' => 'Promotion',
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now()
        	],
        	[
        		'id'=>3,
        		'title' => 'Promotion3',
        		'image_name' => 'p8.jpg',
        		'image_path' => '/uploads/promotion/Oct-2017' ,
        		'description' => 'Promotion',
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now()
        	]
        ];

        Promotion::insert($about);
    }
}
