<?php

use Illuminate\Database\Seeder;
use App\Product;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [            
            [
            'id' => 1,
            'name' => 'Product1',
            'category_id' => 1,            
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'technical' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'price' => 44.00,
            'image_path' => '/uploads/products/Oct-2017',
            'image_name' => 'p1.jpg',
            'created_by' => 1,
            'updated_by' => 1,
            'deleted_at' => NULL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],
            [
            'id' => 2,
            'name' => 'Product2',
            'category_id' => 1,            
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'technical' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'price' => 54.00,
            'image_path' => '/uploads/products/Oct-2017',
            'image_name' => 'p2.jpg',
            'created_by' => 1,
            'updated_by' => 1,
            'deleted_at' => NULL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],
            [
            'id' => 3,
            'name' => 'Product3',
            'category_id' => 1,            
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'technical' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'price' => 64.00,
            'image_path' => '/uploads/products/Oct-2017',
            'image_name' => 'p3.jpg',
            'created_by' => 1,
            'updated_by' => 1,
            'deleted_at' => NULL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],
            [
            'id' => 4,
            'name' => 'Product4',
            'category_id' => 1,            
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'technical' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'price' => 74.00,
            'image_path' => '/uploads/products/Oct-2017',
            'image_name' => 'p4.jpg',
            'created_by' => 1,
            'updated_by' => 1,
            'deleted_at' => NULL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],
            [
            'id' => 5,
            'name' => 'Product5',
            'category_id' => 1,            
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'technical' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'price' => 84.00,
            'image_path' => '/uploads/products/Oct-2017',
            'image_name' => 'p5.jpg',
            'created_by' => 1,
            'updated_by' => 1,
            'deleted_at' => NULL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],
            [
            'id' => 6,
            'name' => 'Product6',
            'category_id' => 1,            
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'technical' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc blandit ultricies ante in auctor. Nunc varius placerat velit quis tempor',
            'price' => 55.00,
            'image_path' => '/uploads/products/Oct-2017',
            'image_name' => 'p6.jpg',
            'created_by' => 1,
            'updated_by' => 1,
            'deleted_at' => NULL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],            
        ];
        Product::insert($products);
    }
}
