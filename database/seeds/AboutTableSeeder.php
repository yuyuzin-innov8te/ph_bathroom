<?php

use Illuminate\Database\Seeder;
use App\About;
use Carbon\Carbon;

class AboutTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $about = [
        	[
        		'id'=>1,
        		'about_us' => '<p>What We Do</p>

				<p>You will be able to find all the top quality swiss mode toilet bowls,sinks,bathtubs,water taps and much more while you are surfing out websites.We want nothing less than a happy experience while you use our products.View some of our more popular products right below this page</p>',
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
        	],
        ];

        About::insert($about);
    }
}
