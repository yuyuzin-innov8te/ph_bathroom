<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
	<div class="left_menu">
		<h3 class="sidebar-title">CATEGORIES</h3>
		<div class="list-group">
			@foreach($categories as $main_category)		

			@if(isset($main_category_id))
			<li class="{{ $main_category->id == $main_category_id ? 'active' : ''}}">
				<a href="{{ url('/product-list').'/'.$main_category->id }}"><i class="fa fa-caret-right" aria-hidden="true"></i>{{ $main_category->name }}</a>
			</li>
			@elseif(isset($product))
			<li class="{{ $main_category->id == $product->category_id ? 'active' : ''}}">
				<a href="{{ url('/product-list').'/'.$main_category->id }}"><i class="fa fa-caret-right" aria-hidden="true"></i>{{ $main_category->name }}</a>
			</li>
			@else
			<li>
				<a href="{{ url('/product-list').'/'.$main_category->id }}"><i class="fa fa-caret-right" aria-hidden="true"></i>{{ $main_category->name }}</a>
			</li>
			@endif
			@endforeach
		</div>		
		<!-- start price range -->
		<?php $current_path = url()->current(); 
		if (strstr($current_path,'product-list')) { ?>
		<h3 class="sidebar-title">PRICE RANGE</h3>
		<div>	
			<div id="slider-range" class="mt30 mb10"></div>
			<div class="mb30">
				<span class="pull-left start-price"></span>
				<span class="pull-right end-price"></span>
			</div>				
				<!-- <p>
					<input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
				</p> -->
			</div>			  
			<?php } ?>

			<!-- end price range -->
		</div> <!-- end left_menu -->
		<div class="clearfix"></div>
		<div class="left_ad">
			<img src="{{ url('/') }}/images/ad.png">
		</div>

</div><!--/.sidebar-offcanvas-->