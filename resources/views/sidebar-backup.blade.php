<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
	<div class="left_menu">
		<h3 class="sidebar-title">CATEGORIES</h3>
		<div class="list-group">
			<?php
			$main_cate = DB::table('collections')
			->select('*')
			->whereNull('sub_collection')
			->whereNull('deleted_at')
			->get();
			?>
			@foreach($main_cate as $k=>$v)
			<?php
			$sub_cate = DB::table('collections')
			->select('*')
			->where('sub_collection','=',$v->id)
			->whereNull('deleted_at')
			->get();
			?> 
			@if($sub_cate)
			<div class="panel panel-default left_menu_panel">
				<div class="panel-heading" role="tab" id="heading_{{ $v->id }}">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $v->id }}" aria-expanded="false" aria-controls="collapse2" class="collapsed">
							{{ $v->name}}
							<i class="pull-right fa fa-caret-down"></i>
						</a>
					</h4>
				</div>

				<div id="collapse_{{ $v->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{ $v->id }}" aria-expanded="false">							
					@foreach($sub_cate as $k=>$v)	
					<div 					
					>						
					<a href="{{ URL::to('product-list/'.$v->id) }}" class="list-group-item">{{ $v->name }}</a>
				</div>
				@endforeach
			</div>
		</div>
		@else
		<li 

		>
		<a href="{{ URL::to('product-list/'.$v->id) }}">{{ $v->name}}								
		</a>							
	</li>

	@endif
	@endforeach

</div>
<h3 class="sidebar-title">PRICE RANGE</h3>
</div> <!-- end left_menu -->
<div class="left_ad">
	<img src="{{ url('/') }}/images/ad.png">
</div>

</div><!--/.sidebar-offcanvas-->