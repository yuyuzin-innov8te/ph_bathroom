@extends('header')




@section('content') 


<div class="row">
	<div class="col-md-12 col-sm-12 contact-text">

	CONTACT US
		
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<img src="{{ url('images/contactus_google.png') }}" class="blog-headerimg">
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 contact-question">

	Feel free to contact us if you have any questions. 
		
	</div>
</div>



<div class="row blog">
<div class="col-md-6"><!-- left  -->
		
					<p>
					<img src="{{ url('images/locat.png') }}" class="">
					
					<span>648 Geylang Rd (Lorong 40) Singapore 389578 </span>
					</p>
					
					<p><img src="{{ url('images/phone.png') }}" class="phone">
					<span class="q-font">+65 6745 1535, +65 6745 1595 </span>
					</p>
					<p><img src="{{ url('images/print.png') }}" class="phone">
					<span class="q-font">+ 6745 6705,</span>
					</p>
					<p>
					<img src="{{ url('images/mail.png') }}" class="locat">
					<span class="footer_ph">ph_bathroom@singnet.com.sg </span>
					</p>
					

					<div style="clear:both;"></div>
					<p>
					<img src="{{ url('images/time.png') }}" class="time-op" >
					<span  class="open-time">Operation Hours:</span><br>
					<span class="open-time">Monday -saturday:</span><span>10:00 am To 8:30 pm</span><br>
					<span class="open-time">Sunday:</span><span >11:00 am to 8:30 pm</span>
					</p>
					
		</div><!-- left end  -->


			<div class="col-md-6"><!-- right start   -->

			<form  action="{{url('sendingemail/')}}" method="post" class="form-horizontal"  >
         		{{ csrf_field() }}

				<div class="form-group">
					<label class="col-md-4 col-sm-4  col-lg-4 col-xs-12 control-label " for="textinput">Name</label>  
					<div class="col-md-8 col-sm-8  col-lg-8 col-sm-12">
						<input id="text" name="name" class="form-control input-md" required="" type="text">

					</div>
				</div>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 col-sm-4  col-lg-4 col-xs-12 control-label" for="textinput">Email</label>  
						<div class="col-md-8 col-sm-8  col-lg-8 col-sm-12">
							<input id="text" class="form-control input-md" name="email" required="" type="text">

						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 col-sm-4  col-lg-4 col-xs-12 control-label" for="textinput">Ph Number</label>  
						<div class="col-md-8 col-sm-8 col-lg-8 col-xs-12 ">
							<input id="text" class="form-control input-md" name="tel"  type="text">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 col-sm-4  col-lg-4 col-xs-12 control-label" for="textinput">Subject</label>  
						<div class="col-md-8 col-sm-8 col-lg-8 col-xs-12 ">
							<input id="text" class="form-control input-md" name="subject"  type="text">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label" for="textinput">Message</label>  
						<div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
							 <textarea class="form-control input-md text-message" name="message" type="text"> </textarea>
							<button class="sent-but">SEND</button>

						</div>
					</div>
				</form>
			</div><!-- right end   -->
</div><!-- row end   -->
@stop
@section('js')

<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function($){

	});

</script>
@stop