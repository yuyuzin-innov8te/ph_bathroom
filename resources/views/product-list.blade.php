@extends('header')

@section('content') 
<div class="row row-offcanvas row-offcanvas-right">					
	@include('sidebar')
	<div class="col-xs-12 col-sm-9">
		<p class="pull-right visible-xs">
			<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
		</p>				
		<div class="row p-filter">
			<!-- <div class="col-md-12"> -->
				<div class="col-md-5">
					<span>Our Products @if(isset($main_category_id)) | {{ \App\Collection::where('id',$main_category_id)->pluck('name')[0] }}@endif</span>
				</div>
				<div class="col-md-7 product_pagination">
					<div class="row">
						<input type="hidden" id="main_category_id" name="main_category_id" value="@if(isset($main_category_id)){{ $main_category_id }}@endif">
						<div class="col-md-5">
							<label>SORTED BY :</label>  						
							<select class="p-select">
								<option value="a_to_z"> NAME(A-Z)</option>
								<option value="z_to_a"> NAME(Z-A)</option>
							</select>
						</div>
						<div class="col-md-4">
							{{ $products->render() }} 
						</div>
						<div class="col-md-3">
							<button class="btn-black" id="view_all">VIEW ALL</button>
						</div>
					</div>					

				</div>
				<!-- </div> -->
			</div>
			<div class="product_row">
				<div class="row">		
					@foreach($products as $product)	
					<div class="col-xs-6 col-lg-4 each_product">
						<div class="product-image">
							<img src="{{ url('/').$product->image_path.'/'.$product->image_name }}">
						</div>						
						<div class="product-caption">
							<a href="{{ url('/')}}/product-detail/{{ $product->id }}" role="button">{{ $product->name }}</a>
						</div>
					</div><!--/.col-xs-6.col-lg-4-->
					@endforeach
				</div><!--/row-->
			</div>			
		</div><!--/.col-xs-12.col-sm-9--> 

	</div><!--/row-->
	<div class="row footer_banner">
		<img src="{{ url('images/footer-banner.png') }}">
	</div>

	@stop
	@section('js')

	<script type="text/javascript" charset="utf-8">
		jQuery(document).ready(function($){
		// Price range //	

		$( function() {

			$( "#slider-range" ).slider({
				range: true,
				min: 0,
				max: 100,
				values: [ 10, 100 ],
				slide: function( event, ui ) {
					// $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );

				$('.start-price').html("$ " + ui.values[ 0 ]);
				$('.end-price').html("$ " + ui.values[ 1 ]);
				}
			});
			// $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			// 	" - $" + $( "#slider-range" ).slider( "values", 1 ) );

			$('.start-price').html("$" + $( "#slider-range" ).slider( "values", 0 ));
			$('.end-price').html("$" + $( "#slider-range" ).slider( "values", 1 ));


			// var loader="<img src='../images/xloading.gif' />";

			$('.ui-slider-handle').on('click',function(){
				// console.log(loader);
				// $('.product_row').html(loader);

				var min_price=$( "#slider-range" ).slider( "values", 0 );
				var max_price=$( "#slider-range" ).slider( "values", 1 );
				var main_category_id = $("#main_category_id").val();


				var qs="min_price="+min_price+"&max_price="+max_price+"&main_category_id="+main_category_id;

				//alert(ctr_id);
		
			$.ajax({
				// url:'ajax/products.php',
				url: '{{ url("product-price-filter") }}',   
				type:'GET',
				data:qs,
				success:function(data){
					// console.log(output);
					// $('.product_row').fadeOut('slow',function(){
					// 	$('.product_row').html(data).fadeIn('fast');						
					// });	

					$('.product_row .row').css('display','none');
					$('.product_row').append(
						'<div class="row bind_row">'+	
						'</div>'
						);

					$.each( data, function( key, value ) {
						$('.product_row .bind_row').append(
							'<div class="col-xs-6 col-lg-4 each_product">'+
							'<div class="product-image">'+
							'<img src="{{ url('/')}}'+value.image_path+'/'+value.image_name+'">'+
							'</div>'+						
							'<div class="product-caption">'+
							'<a href="{{ url('/')}}/product-detail/'+value.id+'" role="button">'+value.name+'</a>'+
							'</div>'+
							'</div>'
						);
					});      				
				}
			});
		});
	});

	// End price range //

	$('.p-select').on('change', function () {
		var filter_by = $(".p-select option:selected").val();
		var main_category_id = $("#main_category_id").val();

		$.ajax({
			type: "POST",
			url: '{{ url("product-filter") }}',          
			data: {
				'filter_by': filter_by,	
				'main_category_id': main_category_id,				              
				"_token": "{{ csrf_token() }}"
			},
			success: function(data) {
				$('.product_row .row').css('display','none');
				$('.product_row').append(
					'<div class="row bind_row">'+	
					'</div>'
					);

				$.each( data, function( key, value ) {
					$('.product_row .bind_row').append(
						'<div class="col-xs-6 col-lg-4 each_product">'+
						'<div class="product-image">'+
						'<img src="{{ url('/')}}'+value.image_path+'/'+value.image_name+'">'+
						'</div>'+						
						'<div class="product-caption">'+
						'<a href="{{ url('/')}}/product-detail/'+value.id+'" role="button">'+value.name+'</a>'+
						'</div>'+
						'</div>'
						);
				});            	
			}
		});
	});

	$('#view_all').on('click', function() {

		var main_category_id = $("#main_category_id").val();
		$.ajax({
			type: "POST",
			url: '{{ url("product-view-all") }}',          
			data: {						
				'main_category_id': main_category_id,				              
				"_token": "{{ csrf_token() }}"
			},
			success: function(data) {
// console.log(data);
$('.product_row .row').css('display','none');
$('.product_row').append(
	'<div class="row bind_row">'+	
	'</div>'
	);

$.each( data, function( key, value ) {
	$('.product_row .bind_row').append(
		'<div class="col-xs-6 col-lg-4 each_product">'+
		'<div class="product-image">'+
		'<img src="{{ url('/')}}'+value.image_path+'/'+value.image_name+'">'+
		'</div>'+						
		'<div class="product-caption">'+
		'<a href="{{ url('/')}}/product-detail/'+value.id+'" role="button">'+value.name+'</a>'+
		'</div>'+
		'</div>'
		);
}); 
}
});
	});
});

</script>
@stop