<div class="container-full footer_wrap">
    <div class="container">
        <p class="pull-left">
           <a href="{{ url('/') }}">                   
                <img src="{{ url('/') }}/images/footer-logo.png">
            </a>
        </p>
        <p class="pull-right">
            &copy; 2017 . PRIVACY POLICY.
        </p>
    </div>
</div>