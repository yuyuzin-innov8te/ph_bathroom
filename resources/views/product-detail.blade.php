@extends('header')

@section('content') 
<div class="row row-offcanvas row-offcanvas-right">					
	@include('sidebar')
	<div class="col-xs-12 col-sm-9">
		<p class="pull-right visible-xs">
			<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
		</p>				

		<div class="row product_gallery">
			<div class="col-md-6">				
				<section id="magnific">
					<div class="row">				
						<div class="col-md-5">
							<div class="xzoom-container">
								<img class="xzoom5" id="xzoom-magnific" src="{{ url('/').$product->image_path.'/'.$product->image_name }}" width="400px" height="400" xoriginal="{{ url('/').$product->image_path.'/'.$product->image_name }}" />

								<div class="xzoom-thumbs">
									<a href="{{ url('/').$product->image_path.'/'.$product->image_name }}">
										<img class="xzoom-gallery5" width="80" height="80" src="{{ url('/').$product->image_path.'/'.$product->image_name }}"  xpreview="{{ url('/').$product->image_path.'/'.$product->image_name }}" title="The description goes here">
									</a>
									@foreach($product_images as $product_image)
									<a href="{{ url('/').$product_image->image_path.'/'.$product_image->image_name }}">
										<img class="xzoom-gallery5" width="80" height="80" src="{{ url('/').$product_image->image_path.'/'.$product_image->image_name }}"  xpreview="{{ url('/').$product_image->image_path.'/'.$product_image->image_name }}" title="The description goes here">
									</a>
									@endforeach
								</div>
							</div>        
						</div>
						<div class="col-md-7"></div>
					</div>
				</section>
			</div>
			<div class="col-md-6">
				<h4>{{ $product->name }}</h4>
				<p>PRICE : <strong> $ {{ $product->price }} </strong></p>

				<!-- start quantity -->
				<div class="row">
					<div class="col-lg-3">
						<p>QUANTITY: </p>
					</div>
					<!-- <div class="col-lg-6"> -->
						<!-- <div class="input-group"> -->
							<span class="">
								<button type="button" class="quantity-left-minus btn-number"  data-type="minus" data-field="">
									<span class="glyphicon glyphicon-minus"></span>
								</button>
							</span>
							<input type="text" id="quantity" name="quantity" class=" input-number" value="1" min="1" max="100">
							<span class="">
								<button type="button" class="quantity-right-plus btn-number" data-type="plus" data-field="">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
							</span>
						<!-- </div> -->
					<!-- </div>				 -->
				</div><!-- end quantity -->
				<div class="row">
					<div class="btn-wrap col-md-12">
						<button class="btn-black">ADD TO CART</button>
						<button class="btn-black">ADD TO WISHLIST</button>
					</div>
					<div class="share-wrap col-md-12">
						<p>Share Us On : <i class="fa fa-twitter" aria-hidden="true"></i> &nbsp; <i class="fa fa-facebook" aria-hidden="true"></i></p>
					</div>					
				</div>
				
			</div>
		</div> <!-- end gallery wrap -->

		<div class="row tab_wrap">
			<div id="responsiveTabsDemo">
				<ul>
					<li>
						<a href="#tab-1">Product Description</a>
					</li>
					<li>
						<a href="#tab-2">Technical Lowdown</a>
					</li>
					<li>
						<a href="#tab-3">Product Questions</a>
					</li>
				</ul>
				<div id="tab-1">
					{{ $product->description }}
				</div>
				<div id="tab-2">
					{{ $product->technical }}
				</div>
				<div id="tab-3">
					{{ $product->question }}
				</div>
			</div><!-- end responsive tab -->		
		</div>
	</div><!--/.col-xs-12.col-sm-9--> 

</div><!--/row-->

<div class="row related_product">
	<div class="col-md-12">
		<h3>Related Products</h3>
		<div class="owl-carousel owl-theme">
			@foreach($related_products as $related_product)	
			<div class="item">
				<div class="product-image">
					<img src="{{ url('/').$related_product->image_path.'/'.$related_product->image_name }}">
				</div>						
				<div class="product-caption">
					<a href="{{ url('/')}}/product-detail/{{ $related_product->id }}" role="button">{{ $related_product->name }}</a>
				</div>
			</div><!--/.col-xs-6.col-lg-4 item-->
			@endforeach
		</div>
	</div>
</div><!-- end related_product -->
<div class="row footer_banner">
	<img src="{{ url('images/footer-banner.png') }}">
</div>

@stop
@section('js')
<script src="{{ url('/') }}/js/setup.js"></script>
<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function($){

		// Quantity increment //

		var quantitiy=0;
		$('.quantity-right-plus').click(function(e){

        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined

        $('#quantity').val(quantity + 1);


        // Increment

    });

		$('.quantity-left-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined

            // Increment
            if(quantity>0){
            	$('#quantity').val(quantity - 1);
            }
        });

		// Product detail slider //

		$(".owl-carousel").owlCarousel({

      	autoPlay: 3000, //Set AutoPlay to 3 seconds
      	items : 4,
      	nav:true,
      	itemsDesktop : [1199,3],
      	itemsDesktopSmall : [979,3]

      });

		$('.owl-prev').text('');
		$('.owl-prev').addClass('fa fa-long-arrow-left');
		$('.owl-next').text('');
		$('.owl-next').addClass('fa fa-long-arrow-right');

		$('#responsiveTabsDemo').responsiveTabs({
			startCollapsed: 'accordion'
		});
	});

</script>
@stop