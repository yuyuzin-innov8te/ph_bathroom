<!DOCTYPE html>
<html>
<head>
	<title>Ph Bathroom</title>

	<link rel="stylesheet" type="text/css" href="{{ url('/')}}/css/bootstrap.min.css"/>
	<link href="https://fonts.googleapis.com/css?family=Roboto|Slabo+27px" rel="stylesheet">

	<link href="{{ url('/')}}/css/offcanvas.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ url('/')}}/css/font-awesome.css"/>
	<link href="{{ url('/') }}/css/owl.carousel.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/css/owl.theme.default.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/css/jquery-ui.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/css/xzoom.css" rel="stylesheet">
	

	<link href="{{ url('/') }}/css/responsive-tabs.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ url('/')}}/css/style.css"/>	
	
	<script src="{{ url('/')}}/js/jquery.min.js"></script> 
	<script src="{{ url('/')}}/js/jquery-ui.min.js"></script> 		
	<script src="{{ url('/') }}/js/xzoom.min.js"></script>
	<script src="{{ url('/')}}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/js/jquery.responsiveTabs.min.js"></script>
	<script src="{{ url('/') }}/js/owl.carousel.min.js"></script>
	<scxript src="{{ url('/') }}/js/offcanvas.js"></script>
	
</head>
<body>
	<header>
		<div class="container">
			<div class="col-md-4">
				<a href="{{ url('/') }}">					
					<img src="{{ url('/') }}/images/logo.png">
				</a>
			</div>
			<div class="col-md-4">
				<div class="search-box">
					<form action="{{ url('/') }}/search-all-products" method="GET">
						<input type="text" value="" class="serach-input" name="search_keyword" placeholder="Search ...">
						<button type="submit" class="searchButton">
							<i class="fa fa-search"></i>
						</button>
					</form>
				</div>
			</div>
			<div class="col-md-4" style="text-align: right;">
				<p>Call Us: 3456789/673456</p>
				<p><i class="fa fa-heart" aria-hidden="true"></i> Wishlist (0) / <i class="fa fa-shopping-bag" aria-hidden="true"></i> My Cart (0)</p>
			</div>
		</div>
	</header>
	<nav class="navbar navbar-inverse" style="margin-bottom:0;">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>        
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="#">Home</a></li>
					<li><a href="{{ url('about-us') }}">About Us</a></li>
					<li {{{ (Request::is('product-detail/*') || (Request::is('product-list/*')) ? 'class=active' : '') }}}>
						<a href="{{url('/product-list')}}">Our Products</a>
					</li>
					<li><a href="{{ url('promotion') }}">Promotions</a></li>
					<li><a href="{{url('contact-us')}}">Contact Us</a></li>
				</ul>
				<ul class="nav navbar-nav pull-right">
					<li><a href="#contact">Login</a></li> 
					<li><a href="#contact">Register</a></li>
				</ul>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</nav><!-- /.navbar -->
		@yield('content-second')
	<div class="container">
		@yield('content')		
	</div><!--/.container-->
  	<!-- Bootstrap core JavaScript
  		================================================== -->

	@yield('js')
	<script type="text/javascript"> 
		$(document).ready(function () {
			var url = window.location.href;
        // passes on every "a" tag
        $("a").each(function() {
                // checks if its the same on the address bar
            if(url == (this.href)) {
            	$(this).closest("a").addClass("active");
            }
        	});
    	});

    </script>

    @extends('footer')
</body>
</html>