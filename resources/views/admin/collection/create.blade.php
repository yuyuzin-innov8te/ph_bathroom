@extends('layouts.app')

@section('content')
    <h3 class="page-title">Category</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.collection.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            Create
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
					<ul id="tree1">
						@foreach($main_categories as $main)
						<?php 
						$sub_cat = App\Collection::where('sub_collection',$main->id)->get();
						?>
						@if(count($sub_cat)>0)

						<li><a href="#" id="{{$main->id}}" class="radio-value">{{$main->name}}</a>
							<ul>
								@foreach($sub_cat as $sub)
								<?php 
								$inner_cat = App\Collection::where('sub_collection',$sub->id)->get();
								?>
								@if(count($inner_cat)>0)
								<li><a href="#" id="{{$main->id}}" class="radio-value">
									{{$sub->name}}</a>
									<ul>
										@foreach($inner_cat as $cat)
										<li>
											<a href="#">
												<div class="radio-success radio ">
													<input type="radio" name="is_available" id="{{$cat->id}}" value="{{$cat->name}}" >
													<label for="{{$cat->id}}" id="{{$cat->id}}"  class="radio-value">{{$cat->name}}
													</label>
												</div>
											</a>
										</li>
										@endforeach
									</ul>
								</li>
								@else
								<li>
									<a href="#">
										<div class="radio-success radio ">
											<input type="radio" name="is_available" id="{{$sub->id}}" value="{{$sub->name}}" >
											<label for="{{$sub->id}}" id="{{$sub->id}}"  class="radio-value">{{$sub->name}}
											</label>
										</div>
									</a>
								</li>
								@endif
								@endforeach
							</ul>
						</li>
						@else
						<li><a href="#">
							<div class="radio-success radio ">
								<input type="radio" name="is_available" id="{{$main->id}}" value="{{$main->name}}" >
								<label for="{{$main->id}}"  class="radio-value" id="{{$main->id}}">{{$main->name}}
								</label>
							</div>
						</a>

					</li>
					@endif
					@endforeach

				</ul>
				</div>
				<div class="col-md-6">
					
					<div class="form-group  category-info">

					</div>
					<div class="form-group" >
					<label class="control-label">Is Collection</label>
						<div class="row">
							<div class="col-md-2">												
								<div class="radio-primary radio">
									<input type="radio" name="is_collection" id="is_collection-0" value="yes">
									<label for="is_collection-0" style="padding-left: 5px;">Yes
									</label>
								</div>
							</div>
							<div class="col-md-10">
								<div class="radio-primary radio">
									<input type="radio" name="is_collection" id="is_collection-1" value="no" checked="checked">
									<label for="is_collection-1" style="padding-left: 5px;">No
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group  is-empty">
						<label  class="control-label">Category Name</label>
						<input type="text" class="form-control"  required="" name="name">
						<input type="hidden" name="sub_category" id="sub_category">
						<span class="help-block">
							<!-- Enter product name. -->
						</span>
					</div>
				</div>
            </div>
            
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
<script>

	$(document).ready(function(){
		$('.radio-value').click(function () {
			$('.radio-value').removeClass('text-success');
			var id = this.id;
			//alert(id);
			$('.category-info').empty();
			var value = $(this).text();
			$(this).addClass('text-success');
			$('.category-info').append('<label  class="control-label text-success" id="label'+id+'">This category name will be under '+value+'.</label> <span class="remove pull-right btn btn-xs btn-danger" id="'+id+'">Remove</span>');
			$('#sub_category').val(id);
			$('.indicator').removeClass('text-success');
			$('input[type=radio]').removeAttr('checked'); 
			$(this).parent().find('.glyphicon-minus-sign').addClass('text-success');
		});

		$(document).on('click', '.remove', function(){
			var id = this.id;
			$('label[id=label'+id+']').remove();
			$('.category-info').append('<label class="control-label text-info">Parent Category Removed.</label>');
			$('#sub_category').val(null);
			this.remove();
		});

	});

	$.fn.extend({
		treed: function (o) {

			var openedClass = 'glyphicon-minus-sign';
			var closedClass = 'glyphicon-plus-sign';

			if (typeof o != 'undefined'){
				if (typeof o.openedClass != 'undefined'){
					openedClass = o.openedClass;
				}
				if (typeof o.closedClass != 'undefined'){
					closedClass = o.closedClass;
				}
			};

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
            	if (this == e.target) {
            		var icon = $(this).children('i:first');
            		icon.toggleClass(openedClass + " " + closedClass);
            		$(this).children().children().toggle();
            	}
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
        tree.find('.branch .indicator').each(function(){
        	$(this).on('click', function () {
        		$(this).closest('li').click();
        	});
        });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
        	$(this).on('click', function (e) {
        		$(this).closest('li').click();
        		e.preventDefault();
        	});
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
        	$(this).on('click', function (e) {
        		$(this).closest('li').click();
        		e.preventDefault();
        	});
        });
    }
});

//Initialization of treeviews

$('#tree1').treed();



</script>
@stop

