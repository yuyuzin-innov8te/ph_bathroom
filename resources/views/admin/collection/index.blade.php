@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Category</h3>
    <p>
        <a href="{{ route('admin.collection.create') }}" class="btn btn-success">Create</a>        
    </p>    

    <div class="panel panel-default">
        <div class="panel-heading">
            List
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($collections) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($collections) > 0)
                        @foreach ($collections as $collection)
                            <tr data-entry-id="{{ $collection->id }}">

                                <td field-key='name'>{{ $collection->name }}</td>
                                <td>
                                    <a href="{{ route('admin.collection.show',[$collection->id]) }}" class="btn btn-xs btn-primary">View</a>                                    
                                    <a href="{{ route('admin.collection.edit',[$collection->id]) }}" class="btn btn-xs btn-info">Edit</a>
									{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.collection.destroy', $collection->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('user_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.users.mass_destroy') }}';
        @endcan

    </script>
@endsection