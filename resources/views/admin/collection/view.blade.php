@extends('admin.header')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="dashboard_graph">
			<div class="row">
				<div class="col-md-12 x_title col-xs-12">
					<h2>View Category</h2>
					
				</div>
				
			</div>
			
				<div class="row">
					<div class="col-md-6">

						<div class="form-group  is-empty">
							<label  class="control-label">Category Name</label>
							<input type="text" class="form-control"  required="" name="name" value="{{$collections->name}}" readonly="">
							<span class="help-block">
								<!-- Enter product name. -->
							</span>
						</div>
	
					</div>

					
				</div>

				<div class=" row">
					<div class="col-md-6">
						<a href="{{asset('/admin/collection')}}" class="btn btn-info">Cancel</a>
						<a href="{{asset('/admin/collection/'.$collections->id.'/edit')}}" class="btn btn-warning pull-right"><i class="fa fa-edit"></i> Edit</a>
						
					</div>
				</div>

			
		</div>
	</div>

</div>
@endsection