@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Promotion</h3>
    <p>
        <a href="{{ route('admin.promotion.create') }}" class="btn btn-success">Create</a>        
    </p>    

    <div class="panel panel-default">
        <div class="panel-heading">
            List
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($promotions) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Promotion</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($promotions) > 0)
                        @foreach ($promotions as $promotion)
                            <tr data-entry-id="{{ $promotion->id }}">
                                <td field-key='title'><?php echo $promotion->title ?></td>
                                <td>
                                    <a href="{{ route('admin.promotion.show',[$promotion->id]) }}" class="btn btn-xs btn-primary">View</a>                                    
                                    <a href="{{ route('admin.promotion.edit',[$promotion->id]) }}" class="btn btn-xs btn-info">Edit</a>
									{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.promotion.destroy', $promotion->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('user_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.users.mass_destroy') }}';
        @endcan
        
    </script>
@endsection