@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{asset('/css/bootstrap-fileupload.min.css')}}" />
@endsection
@section('content')
    <h3 class="page-title">Promotion</h3>

    {!! Form::model($promotion, ['method' => 'PUT', 'route' => ['admin.promotion.update', $promotion->id], 'enctype' => 'multipart/form-data']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('title', 'Title*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::text('title', $promotion->title, ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('title'))
                            <p class="help-block">
                                {{ $errors->first('title') }}
                            </p>
                        @endif
                    </div>
                </div>
                <!-- <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('price', 'Price*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::text('price', $promotion->price, ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('price'))
                            <p class="help-block">
                                {{ $errors->first('price') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('percent', 'Percent*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::text('percent', $promotion->percent, ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('percent'))
                            <p class="help-block">
                                {{ $errors->first('percent') }}
                            </p>
                        @endif
                    </div>
                </div> -->
                
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('image', 'Image*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img class="index-img" src='{{asset("$promotion->image_path/$promotion->image_name")}}'/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-file btn-primary">
                                    <span class="fileupload-new">Select image</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input name="image" type="file" />
                                </span>
                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                        <p class="help-block"></p>
                        @if($errors->has('image'))
                            <p class="help-block">
                                {{ $errors->first('image') }}
                            </p>
                        @endif
                    </div>                    
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::textarea('description', $promotion->description, ['class' => 'form-control editor', 'placeholder' => '']) !!}
                        
                    </div>                   
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
<script src="{{asset('/js/bootstrap-fileupload.js')}}"></script>
<script src="{{asset('/libs/ckeditor/ckeditor.js')}}"></script>

<script>
    $(function () {
        CKEDITOR.replaceClass = 'editor';
    });
</script>
@endsection

