@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{asset('/css/bootstrap-fileupload.min.css')}}" />
@endsection
@section('content')
    <h3 class="page-title">Promotion</h3>
    

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>
        
        <div class="panel-body">
            <div class="row">
                
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-8">
                        <?php echo $promotion->title; ?>
                    </div>                   
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('price', 'Price', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-8">
                        <?php echo $promotion->price; ?>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('percent', 'Percent', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-8">
                        <?php echo $promotion->percent; ?>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-8">
                        <?php echo $promotion->description; ?>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-8">
                         <img class="index-img" src='{{asset("$promotion->image_path/$promotion->image_name")}}' width="200px" height="150px" />
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

