@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{asset('/css/bootstrap-fileupload.min.css')}}" />
@endsection
@section('content')
    <h3 class="page-title">About us</h3>
    

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>
        
        <div class="panel-body">
            <div class="row">
                
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('about_us', 'About us', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-8">
                        <?php echo $about->about_us; ?>
                    </div>                   
                </div>

            </div>
        </div>
    </div>
@stop

