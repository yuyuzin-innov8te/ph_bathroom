@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{asset('/css/bootstrap-fileupload.min.css')}}" />
@endsection
@section('content')
    <h3 class="page-title">Product</h3>
    {!! Form::model($about,['method' => 'PUT', 'route' => ['admin.about.update',$about->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>
        
        <div class="panel-body">
            <div class="row">
                
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('about_us', 'About us*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-8">
                        {!! Form::textarea('about_us', $about->about_us, ['class' => 'form-control editor', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('about_us'))
                            <p class="help-block">
                                {{ $errors->first('about_us') }}
                            </p>
                        @endif
                    </div>                   
                </div>

            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
<script src="{{asset('/libs/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
    $(function () {
        CKEDITOR.replaceClass = 'editor';
    });
</script>
@endsection

