@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">About Us</h3>
    <p>
        <a href="{{ route('admin.about.create') }}" class="btn btn-success">Create</a>        
    </p>    

    <div class="panel panel-default">
        <div class="panel-heading">
            List
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($abouts) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>About Us</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($abouts) > 0)
                        @foreach ($abouts as $about)
                            <tr data-entry-id="{{ $about->id }}">
                                <td field-key='name'><?php echo $about->about_us ?></td>
                                <td>
                                    <a href="{{ route('admin.about.show',[$about->id]) }}" class="btn btn-xs btn-primary">View</a>                                    
                                    <a href="{{ route('admin.about.edit',[$about->id]) }}" class="btn btn-xs btn-info">Edit</a>
									{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.about.destroy', $about->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('user_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.users.mass_destroy') }}';
        @endcan
        
    </script>
@endsection