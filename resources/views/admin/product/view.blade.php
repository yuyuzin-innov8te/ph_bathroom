@extends('layouts.app')
@section('css')

@endsection
@section('content')
    <h3 class="page-title">Product</h3>

    {!! Form::model($product, ['method' => 'PUT', 'route' => ['admin.product.update', $product->id], 'enctype' => 'multipart/form-data']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {{$product->name}}
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('category', 'Category', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {{$product->category->name}}
                    </div>                    
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('price', 'Price', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {{$product->price}}
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        <?php echo $product->description; ?>
                    </div>                   
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('technical', 'Technical Lowdown', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        <?php echo $product->technical; ?>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('question', ' Question', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        <?php echo $product->question; ?>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('image', 'Main Image', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        <img class="index-img" src='{{asset("$product->image_path/$product->image_name")}}' width="200px" height="150px" />
                    </div>                    
                </div>
                <br>
                <hr>
                <div class="col-xs-12 form-group">
                    <div class="col-md-3">
                        {!! Form::label('image', 'Other Image', ['class' => 'control-label']) !!}
                    </div>
                </div>

                @foreach($product_image as $value)
                    <div class="col-xs-12 form-group">
                        <div class="col-xs-3">            
                        </div>
                        <div class="col-xs-6">
                        <img class="index-img" src='{{asset("$value->image_path/$value->image_name")}}' width="200px" height="150px"/>
                    </div>              
                    </div>
                @endforeach
                
                <div id="team_wrap"></div>

            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')


<script type="text/html" id="add_team_html">
     <div class="team">    
    <div class="col-xs-12 form-group">
        <div class="col-xs-3">            
        </div>
        <div class="col-xs-4">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img class="index-img" src=""/>
                </div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                <div>
                    <span class="btn btn-file btn-primary">
                        <span class="fileupload-new">Select image</span>
                        <span class="fileupload-exists">Change</span>
                        <input name="other_image[]" type="file" />
                    </span>
                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                </div>
            </div>
            <p class="help-block"></p>
            @if($errors->has('other_image'))
                <p class="help-block">
                    {{ $errors->first('other_image') }}
                </p>
            @endif
        </div>  
        <div class="col-md-1">
            <button type="button" class="btn btn-danger remove_team">
                <i class="fa fa-close"></i>
            </button>
        </div>                  
    </div>
    <hr>
</div>   
</script>

<script>
    $(function () {
        CKEDITOR.replaceClass = 'editor';
    });

    $("#team_wrap").on("click", ".remove_team", function(){
        $(this).parents('.team').remove();
    });

    function addTeam()
    {
        $('#team_wrap').append($('#add_team_html').html());
    }
</script>
@endsection

