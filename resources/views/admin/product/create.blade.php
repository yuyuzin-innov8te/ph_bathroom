@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{asset('/css/bootstrap-fileupload.min.css')}}" />
@endsection
@section('content')
    <h3 class="page-title">Product</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.product.store'], 'enctype' => 'multipart/form-data']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('name'))
                            <p class="help-block">
                                {{ $errors->first('name') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('category', 'Category*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::select('category_id', $categories, old('category_id'), ['class' => 'form-control select2']) !!}
                    </div>                    
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('price', 'Price*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::text('price', old('price'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('price'))
                            <p class="help-block">
                                {{ $errors->first('price') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::textarea('description', old('description'), ['class' => 'form-control editor', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('description'))
                            <p class="help-block">
                                {{ $errors->first('description') }}
                            </p>
                        @endif
                    </div>                   
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('technical', 'Technical Lowdown*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::textarea('technical', old('technical'), ['class' => 'form-control editor', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('technical'))
                            <p class="help-block">
                                {{ $errors->first('technical') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('question', ' Question*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::textarea('question', old('question'), ['class' => 'form-control editor', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('question'))
                            <p class="help-block">
                                {{ $errors->first('question') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3">
                        {!! Form::label('image', 'Main Image*', ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-xs-6">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img class="index-img" src=""/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-file btn-primary">
                                    <span class="fileupload-new">Select image</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input name="main_image" type="file" />
                                </span>
                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                        <p class="help-block"></p>
                        @if($errors->has('main_image'))
                            <p class="help-block">
                                {{ $errors->first('main_image') }}
                            </p>
                        @endif
                    </div>                    
                </div>
                <br>
                <hr>
                <div class="form-group">
                    <div class="col-md-3">
                        <button type="button" onclick="addTeam()" class="btn btn-bg btn-info">Add Other Image</button>
                    </div>
                </div>

                <div id="team_wrap"></div>

            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
<script src="{{asset('/js/bootstrap-fileupload.js')}}"></script>
<script src="{{asset('/libs/ckeditor/ckeditor.js')}}"></script>

<script type="text/html" id="add_team_html">
  <div class="team">    
    <div class="col-xs-12 form-group">
        <div class="col-xs-3">            
        </div>
        <div class="col-xs-4">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img class="index-img" src=""/>
                </div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                <div>
                    <span class="btn btn-file btn-primary">
                        <span class="fileupload-new">Select image</span>
                        <span class="fileupload-exists">Change</span>
                        <input name="other_image[]" type="file" />
                    </span>
                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                </div>
            </div>
            <p class="help-block"></p>
            @if($errors->has('other_image'))
                <p class="help-block">
                    {{ $errors->first('other_image') }}
                </p>
            @endif
        </div>  
        <div class="col-md-1">
            <button type="button" class="btn btn-danger remove_team">
                <i class="fa fa-close"></i>
            </button>
        </div>                  
    </div>
    <hr>
</div>
</script>

<script>
    $(function () {
        CKEDITOR.replaceClass = 'editor';
    });

    addTeam();

    $("#team_wrap").on("click", ".remove_team", function(){
        $(this).parents('.team').remove();
    });

    function addTeam()
    {
        $('#team_wrap').append($('#add_team_html').html());
    }
</script>
@endsection

